# Coding Challenge

_immocloud ist bei den Bewerbern sehr beliebt. Wir brauchen einen Überblick über die aktuellen Bewerber. Entwickle die
passende Web-Anwendung dazu._

Default Projektausführung mit Frontendport 5173 und Backendport 8080.

## Offene TODOs

- Axios handling für HTTP-Fehlerstati und Exceptions.
- Update des Status erfordert Neuladen des gesamten Datenbestandes. Besser nur die einzelne Entität wird neugeladen.
- CORS Nutzung via Spring Security lösen.
- Nutzung der Entity Klassen nicht bis in den Controller durchreichen.  
  Schichtenweise eigene Transfer-Objekte nutzen.  
  (Z.B. Die Nutzung einer Entity Klasse im WebController ist schlecht wenn sich die Web-Schnittstelle ändert. Das darf
  keine Auswirkung auf die Entityschicht haben. Selbiges gilt auch umgekehrt bzw. für alle Schichten untereinander. )
- Tests schreiben ;-)
- Stringente Nutzung von Typescript

## Web / VueJS

    npm install
    npm run format
    npm run dev

## MongoDB

Docker Kommando zur Erstellung und Ausführung des Containers als Image.  
MongoDB ist auf Port 27017 zugreifbar.

### Image

`docker pull mongo:6.0`

### Container

Beispiel (m)einer lokalen Installation/Nutzung.  

`docker run --hostname=2cab10b590b4 
            --mac-address=02:42:ac:11:00:02
            --env=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin 
            --env=GOSU_VERSION=1.16
            --env=JSYAML_VERSION=3.13.1
            --env=MONGO_PACKAGE=mongodb-org
            --env=MONGO_REPO=repo.mongodb.org
            --env=MONGO_MAJOR=6.0
            --env=MONGO_VERSION=6.0.10
            --env=HOME=/data/db --volume=/data/configdb
            --volume=/data/db
            --label='org.opencontainers.image.ref.name=ubuntu'
            --label='org.opencontainers.image.version=22.04'
            --runtime=runc 
            -d -p 27017:27017 
            mongo:6.0`