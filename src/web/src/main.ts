import './assets/main.css'

import { createApp } from 'vue'
import mitt from 'mitt';
import App from './App.vue'
import Axios from "axios";

Axios.defaults.baseURL = 'http://localhost:8080/';

const emitter = mitt();

const app = createApp(App).provide( 'emitter', emitter );
app.config.globalProperties.emitter = emitter;
app.mount('#app');

export default {
    setup() {
        //provide('emitter', emitter);
    }
}