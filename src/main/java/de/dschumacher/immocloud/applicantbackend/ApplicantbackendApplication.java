package de.dschumacher.immocloud.applicantbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ApplicantbackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApplicantbackendApplication.class, args);
    }
}
