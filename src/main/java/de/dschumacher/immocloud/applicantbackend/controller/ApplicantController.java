package de.dschumacher.immocloud.applicantbackend.controller;

import de.dschumacher.immocloud.applicantbackend.nosql.Status;
import de.dschumacher.immocloud.applicantbackend.service.ApplicantsService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ApplicantController {

    private final ApplicantsService applicantsService;

    public ApplicantController(ApplicantsService applicantsService) {
        this.applicantsService = applicantsService;
    }

    @GetMapping(value = "/applicants", produces = MediaType.APPLICATION_JSON_VALUE)
    // TODO: Cors Nutzung über Spring-Security lösen
    @CrossOrigin(origins = "http://localhost:5173")
    public ResponseEntity<List<ApplicantResponse>> list() {

        final var applicants = applicantsService.list().stream()
                .map(applicant -> new ApplicantResponse(applicant.getId(),
                        applicant.getFirstname()+" "+applicant.getLastname(),
                        applicant.getStatus()))
                .toList();

        return ResponseEntity.ok(applicants);
    }

    @PostMapping("/add")
    // TODO: Cors Nutzung über Spring-Security lösen
    @CrossOrigin(origins = "http://localhost:5173")
    public void add(@RequestBody NewNameRequest newNameRequest) {

        Assert.notNull(newNameRequest, "No request object given");

        applicantsService.add(newNameRequest.getName());
    }

    /**
     * Setzt den Status eines identifizierten Eintrages von OPEN auf ACCEPTED oder DECLINED
     */
    @PostMapping("/accept/{id}/{value}")
    // TODO: Cors Nutzung über Spring-Security lösen
    @CrossOrigin(origins = "http://localhost:5173")
    public void changeStatus(@PathVariable("id") String id, @PathVariable("value") boolean isAccepted) {
        this.applicantsService.changeStatus(id, isAccepted);
    }

    /**
     * Web Transfer Object representing applicants and their status
     */
    @AllArgsConstructor
    @Getter
    public static class ApplicantResponse {
        String id;
        String name;
        Status status;
    }

    @Data
    public static class NewNameRequest {
        String name;
    }
}
