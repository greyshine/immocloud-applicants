package de.dschumacher.immocloud.applicantbackend.nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicantRepository extends MongoRepository<ApplicantEntity, String> {

    /**
     * @param id     des Bewerbers
     * @param status neu zu setzender {@link Status}
     * @return Anzahl der geänderten Dokumente
     */
    @Query("{'id' : ?0}")
    @Update("{'$set': {'status': ?1}}")
    Integer updateStatus(String id, Status status);

}
