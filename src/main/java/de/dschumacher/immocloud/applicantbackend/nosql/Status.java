package de.dschumacher.immocloud.applicantbackend.nosql;

/**
 * Status of an applicant of being employed
 */
public enum Status {
    OPEN,
    ACCEPTED,
    DECLINED
}
