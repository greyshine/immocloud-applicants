package de.dschumacher.immocloud.applicantbackend.nosql;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "applicants")
@Builder
@Data
public class ApplicantEntity {

    @Id
    private String id;
    private String firstname;
    private String lastname;
    private Status status;
}
