package de.dschumacher.immocloud.applicantbackend.service;

import de.dschumacher.immocloud.applicantbackend.nosql.ApplicantEntity;
import de.dschumacher.immocloud.applicantbackend.nosql.ApplicantRepository;
import de.dschumacher.immocloud.applicantbackend.nosql.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ApplicantsService {

    private final ApplicantRepository applicantRepository;

    public ApplicantsService(ApplicantRepository applicantRepository) {
        this.applicantRepository = applicantRepository;
    }

    /**
     * @return Sammlung der {@link ApplicantEntity}s
     */
    public List<ApplicantEntity> list() {
        return applicantRepository.findAll(Sort.by("name") );
    }

    /**
     * Fügt einen neuen Applicant hinzu und vergibt den Status OPEN
     *
     * @param name des neuen Bewerbers.
     */
    public void add(String name) {

        Assert.isTrue( name != null, "name is null" );
        Assert.isTrue( !name.trim().isBlank(), "name is blank" );

        final var nameTo = buildName(name);

        var applicantEntity = ApplicantEntity.builder()
                .firstname(nameTo.getFirstname())
                .lastname(nameTo.getLastname())
                .status(Status.OPEN)
                .build();

        applicantEntity = applicantRepository.save(applicantEntity);
        log.info("Stored: {}", applicantEntity);
    }

    /**
     * Ändert den Status eines Bewerbers auf ACCPTED oder DECLINED
     *
     * @param id         des Bewerbers
     * @param isAccepted true für Status ACCEPTED, false für Status DECLINED
     */
    public void changeStatus(String id, boolean isAccepted) {

        Assert.isTrue(id != null, "Id must not be blank");
        Assert.isTrue(!id.trim().isBlank(), "Id must not be blank");

        final var status = isAccepted ? Status.ACCEPTED : Status.DECLINED;
        final var changedDocuments = this.applicantRepository.updateStatus(id, status);

        Assert.isTrue(changedDocuments > 0, "Keine Status Änderung erfolgt [id=" + id + ", neuer Status=" + status + "]");
    }

    private static Name buildName(String name) {

        Assert.isTrue( name != null, "name is null" );
        Assert.isTrue( !name.trim().isBlank(), "name is blank" );

        name = name.trim();

        final int idx = name.indexOf(" ");
        Assert.isTrue( idx > -1, "Name must consist of first and lastname" );

        final var firstname = name.substring(0,idx).trim();
        final var lastname = name.substring(idx).trim();

        return new Name(firstname, lastname);
    }

    /**
     * TO für Vor- und Nachname
     */
    @AllArgsConstructor
    @Data
    private static class Name {
        String firstname;
        String lastname;
    }
}
